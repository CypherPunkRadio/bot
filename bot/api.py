import aiohttp

import settings
from bot import TG_API


class API:

    base_url = f'http://{settings.API_HOST}:{settings.API_PORT}'

    @classmethod
    async def get(cls):
        async with aiohttp.ClientSession() as session:
            async with session.get(f'{cls.base_url}/api/all') as resp:
                response = await resp.json()
                return response, resp.status

    @classmethod
    async def search(cls, title):
        async with aiohttp.ClientSession() as session:
            async with session.get(f'{cls.base_url}/api/search/{title}') as resp:
                response = await resp.json()
                return response, resp.status

    @classmethod
    async def add(cls, song_id, update, context):
        api = TG_API(update, context)
        async with aiohttp.ClientSession() as session:
            await api.send_message('<b>Adding song...</b>')
            async with session.put(f'{cls.base_url}/api/manage/{settings.API_SECRET}/add/{song_id}') as resp:
                response = await resp.json()
                return response, resp.status

    @classmethod
    async def delete(cls, song_id, update, context):
        api = TG_API(update, context)
        async with aiohttp.ClientSession() as session:
            await api.send_message('<b>Deleting song...</b>')
            async with session.delete(f'{cls.base_url}/api/manage/{settings.API_SECRET}/delete/{song_id}') as resp:
                response = await resp.json()
                return response, resp.status

    @classmethod
    async def current_song(cls):
        async with aiohttp.ClientSession() as session:
            async with session.get(f'{cls.base_url}/api/song') as resp:
                response = await resp.json()
                return response

    @classmethod
    async def listeners(cls):
        async with aiohttp.ClientSession() as session:
            async with session.get(f'{cls.base_url}/api/listeners') as resp:
                response = await resp.json()
                return response
