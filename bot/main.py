import logging

from telegram.ext import ApplicationBuilder, CommandHandler, ConversationHandler, CallbackQueryHandler
from telegram import Update
from telegram.ext import ContextTypes
from telegram_bot_pagination import InlineKeyboardPaginator

import settings
from bot import commands
from bot.tg_api import TG_API
from bot.utils import UTILS

END = ConversationHandler.END

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)

application = ApplicationBuilder().proxy_url(settings.PROXY).token(settings.TOKEN).build()
start_handler = CommandHandler('start', commands.start_command)
help_handler = CommandHandler('help', commands.help_command)
community_handler = CommandHandler('community', commands.community_command)
current_song_handler = CommandHandler('song', commands.current_song_command)
listeners_handler = CommandHandler('listeners', commands.listeners_command)
list_handler = CommandHandler('list', commands.list_command)
search_handler = CommandHandler('search', commands.search_command)
add_handler = CommandHandler('add', commands.add_command)
delete_handler = CommandHandler('delete', commands.delete_command)

async def get_list(update: Update, context: ContextTypes.DEFAULT_TYPE):
    api = TG_API(update, context)
    query = update.callback_query

    await query.answer()

    print(query.data)

    page = int(query.data.split('#')[1])

    songs, total = await UTILS.get_all()

    msg = await UTILS.format_msg(songs, page, total)

    paginator = InlineKeyboardPaginator(
        int(total/int(settings.SONGS_CHUNK)),
        current_page=page,
        data_pattern='page#{page}'
    )

    return await api.edit_message(msg, update.effective_message.id, reply_markup=paginator.markup)