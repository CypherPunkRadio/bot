import settings
from bot.api import API


class UTILS:

    @staticmethod
    async def get_all():

        songs = await API.get()

        songs = songs[0]

        total = len(songs)

        return songs, total

    @staticmethod
    async def format_msg(songs: dict, page: int, total: int):
        msg: str = ''
        chunk: int = int(settings.SONGS_CHUNK)

        end: int = int(chunk * page)

        start: int = end - chunk

        if page == 1:
            start = 0
            end = chunk
            msg = f"<b>There are '{total}' songs in the database!</b>"

        songs = list(songs.items())[start:end]

        for song in songs:
            song_data = song[1]
            song_id = song[0]
            date = song_data.get('added')
            title = song_data.get('title')
            msg = msg + f'''
        <b><a href="https://yewtu.be/watch?v={song_id}">{title}</a></b>
            &#10551;ID: <code>{song_id}</code>
            &#10551;added: {date}
                    '''

        return msg
