from telegram import Update
from telegram.ext import ContextTypes
from telegram_bot_pagination import InlineKeyboardPaginator

import settings
from bot import restricted
from bot.api import API
from bot.tg_api import TG_API
from bot.utils import UTILS

LIST, CLOSE = range(0, 2)


async def start_command(update: Update, context: ContextTypes.DEFAULT_TYPE):
    api = TG_API(update, context)
    await api.send_message('''Welcome <b>CypherPunk</b>!
This is the CypherPunk Radio's bot
For more info and commands, /help
Join our chats, /community
<code>Live, private, secure & unstoppable.</code>     
    ''')


async def help_command(update: Update, context: ContextTypes.DEFAULT_TYPE):
    api = TG_API(update, context)
    await api.send_message('''<b>Help!</b>
/community to connect with us
/add <code>'id'</code> to add a new song
<b>[IDs must be taken from YouTube ONLY]</b>
/song get currently playing song
/listeners get live listeners
/list get all songs
/search <code>'title'</code> search for a song
    ''')


async def community_command(update: Update, context: ContextTypes.DEFAULT_TYPE):
    api = TG_API(update, context)
    await api.send_message('''<b>Community</b>
Website: <a href="https://cypherpunkradio.com">landing</a>
Telegram: @cypherpunkradio
XMPP: cypherpunk-radio@muc.xmpp.is
IRC: <a href="webchat.oftc.net/?channels=cypherpunk-radio">OFTC</a>
    ''')


async def list_command(update: Update, context: ContextTypes.DEFAULT_TYPE):
    api = TG_API(update, context)

    songs, total = await UTILS.get_all()

    msg = await UTILS.format_msg(songs, 1, total)

    paginator = InlineKeyboardPaginator(
        int(total/int(settings.SONGS_CHUNK)),
        current_page=1,
        data_pattern='page#{page}'
    )

    return await api.send_message(msg, reply_markup=paginator.markup)


async def current_song_command(update: Update, context: ContextTypes.DEFAULT_TYPE):
    api = TG_API(update, context)

    song: dict = await API.current_song()

    song, song_id, date = song.get('current_song').values()

    return await api.send_message(f'''<b>Now playing:</b>
<a href='https://yewtu.be/watch?v={song_id}'>{song}</a> 
    &#10551;ID: {song_id}
    &#10551;added: {date}
    
Get all songs with /list 
    ''')


async def listeners_command(update: Update, context: ContextTypes.DEFAULT_TYPE):
    api = TG_API(update, context)

    listeners = await API.listeners()

    listeners = listeners.get('listeners')

    return await api.send_message(f'''<i>{listeners}</i><b> users are listening right now!</b>
    ''')


async def search_command(update: Update, context: ContextTypes.DEFAULT_TYPE):
    api = TG_API(update, context)

    try:
        title = update.message.text.split(' ')[1]
    except IndexError:
        return await api.send_message(f'<b>Error: Empty ID!</b>')

    response = await API.search(title)

    response = response[0]

    if not response:
        return await api.send_message(f'<b>Error: Song not found!</b>')

    song_id = list(response.keys())[0]

    response = response.get(song_id)

    return await api.send_message(f'''<b>Song found!</b>
Title: <a href="https://yewtu.be/watch?v={song_id}">{response.get('title')}</a>
ID: <code>{song_id}</code>
Added: {response.get('added')}
    ''')


async def add_command(update: Update, context: ContextTypes.DEFAULT_TYPE):
    api = TG_API(update, context)

    msg_id = update.message.message_id + 1

    try:
        song_id = update.message.text.split(' ')[1]
    except IndexError:
        return await api.send_message(f'<b>Error: Empty ID!</b>')

    response, code = await API.add(song_id, update, context)

    if code == 429:
        return await api.edit_message(f'<b>Error: Too many requests!</b>', msg_id=msg_id)
    elif resp := response.get('error'):
        return await api.edit_message(f'<b>Error: {list(resp.values())[0]}</b>', msg_id=msg_id)

    return await api.edit_message(f'''<b>Song added!</b>
<code>/search {response.get(song_id).get('title')}</code>
    ''', msg_id=msg_id)


@restricted
async def delete_command(update: Update, context: ContextTypes.DEFAULT_TYPE):
    api = TG_API(update, context)

    try:
        song_id = update.message.text.split(' ')[1]
    except IndexError:
        return await api.send_message(f'<b>Error: Empty ID!</b>')

    response, code = await API.delete(song_id, update, context)

    msg_id = update.message.message_id + 1

    if resp := response.get('error'):
        return await api.edit_message(f'<b>Error: {list(resp.values())[0]}</b>', msg_id=msg_id)

    return await api.edit_message('<b>Song deleted!</b>', msg_id=msg_id)
