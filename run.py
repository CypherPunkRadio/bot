from bot.main import application, start_handler, help_handler, community_handler, add_handler, search_handler, \
    list_handler, current_song_handler, listeners_handler, delete_handler

from telegram.ext import CallbackQueryHandler

from bot.main import get_list

if __name__ == '__main__':

    application.add_handler(start_handler)
    application.add_handler(help_handler)
    application.add_handler(community_handler)
    application.add_handler(current_song_handler)
    application.add_handler(listeners_handler)
    application.add_handler(list_handler)
    application.add_handler(search_handler)
    application.add_handler(add_handler)
    application.add_handler(delete_handler)
    application.add_handler(CallbackQueryHandler(get_list, pattern='^page#'))

    application.run_polling()
