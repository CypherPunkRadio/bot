from dotenv import load_dotenv
import os

load_dotenv()

TOKEN = os.environ.get('TOKEN')
PROXY = 'socks5://127.0.0.1:9050'

API_SECRET = os.environ.get('API_SECRET')

API_HOST = os.environ.get('API_HOST')
API_PORT = os.environ.get('API_PORT')

LIST_OF_ADMINS = os.environ.get('ADMINS').split(',')  # .split(',') if list of admins

SONGS_CHUNK = os.environ.get('SONGS_CHUNK')